def main():
    print("Give God Thanks And Trust In Him.")
    tithe = input("What is your base income: ")
    tithe_calc(tithe)

def tithe_calc(perc):
    tithing = float(float(perc) * 0.10)
    tithe_per_paycheck = tithing / 2
    print(f"The total amount of tithing is: {tithing:.1f}")
    print(f"Tithing amount per paycheck: {tithe_per_paycheck:.1f}")
    print("Praise The Lord, He Is Worthy")
    

main()
